"""Data models for the application."""

from pydantic import BaseModel, Field, AliasChoices
from datetime import datetime, timedelta


class Forecast(BaseModel):
    """Base Forecast class for a time step."""
    time: datetime = Field(validation_alias=AliasChoices("forecastTime", "time"))
    maxScreenAirTemp: float
    minScreenAirTemp: float
    feelsLikeTemp: float
    significantWeatherCode: int
    probOfPrecipitation: float
    totalPrecipAmount: float
    totalSnowAmount: float
    windSpeed10m: float
    windDirectionFrom10m: int
    windGustSpeed10m: float
    max10mWindGust: float
    visibility: int
    mslp: int
    uvIndex: int
    screenRelativeHumidity: float

    @staticmethod
    def temp_colour(temp) -> str:
        if temp < 3:
            return "--temp-cold"
        if temp < 10:
            return "--temp-cool"
        if temp < 16:
            return "--temp-mild"
        if temp <= 22:
            return "--temp-warm"
        if temp > 22:
            return "--temp-hot"
        return "--body-background"


    def next_time_step(self, increment: int) -> str:
        _next = self.time + timedelta(hours=increment)
        return _next.strftime("%Y-%m-%d %H:00:00+00:00")

    def previous_time_step(self, increment: int) -> str:
        _previous = self.time - timedelta(hours=increment)
        return _previous.strftime("%Y-%m-%d %H:00:00+00:00")

    def cardinal_direction(self) -> str:
        compass_points = ["N", "NNE", "NE", "ENE", "E", "ESE",
                          "SE", "SSE", "S", "SSW", "SW", "WSW",
                          "W", "WNW", "NW", "NNW", "N"]
        sectors = 16
        index = round((self.windDirectionFrom10m % 360) / (360 / sectors))
        return compass_points[index]

    def significant_weather_icon(self) -> str:
        sig_code = {
            "NA": "Not available",
            "-1": "Trace rain",
            "0": "00-clear-night.png",
            "1": "01-sunny-day.png",
            "2": "02-partly-cloudy-night.png",
            "3": "03-partly-cloudy-day.png",
            "4": "",
            "5": "",
            "6": "",
            "7": "07-cloudy.png",
            "8": "08-overcast.png",
            "9": "12-light-rain.png",
            "10": "10-light-rain-shower-day.png",
            "11": "11-drizzle.png",
            "12": "12-light-rain.png",
            "13": "15-heavy-rain.png",
            "14": "15-heavy-rain.png",
            "15": "15-heavy-rain.png",
            "16": "18-sleet.png",
            "17": "18-sleet.png",
            "18": "18-sleet.png",
            "19": "21-hail.png",
            "20": "21-hail.png",
            "21": "21-hail.png",
            "22": "24-light-snow.png",
            "23": "24-light-snow.png",
            "24": "24-light-snow.png",
            "25": "27-heavy-snow.png",
            "26": "27-heavy-snow.png",
            "27": "27-heavy-snow.png",
            "28": "30-thunder.png",
            "29": "30-thunder.png",
            "30": "30-thunder.png"
        }
        return sig_code.get(str(self.significantWeatherCode))

    def significant_weather_code(self) -> str:
        sig_code = {
            "NA": "Not available",
            "-1": "Trace rain",
            "0": "Clear night",
            "1": "Sunny day",
            "2": "Partly cloudy (night)",
            "3": "Partly cloudy (day)",
            "4": "Not used",
            "5": "Mist",
            "6": "Fog",
            "7": "Cloudy",
            "8": "Overcast",
            "9": "Light rain shower (night)",
            "10": "Light rain shower (day)",
            "11": "Drizzle",
            "12": "Light rain",
            "13": "Heavy rain shower (night)",
            "14": "Heavy rain shower (day)",
            "15": "Heavy rain",
            "16": "Sleet shower (night)",
            "17": "Sleet shower (day)",
            "18": "Sleet",
            "19": "Hail shower (night)",
            "20": "Hail shower (day)",
            "21": "Hail",
            "22": "Light snow shower (night)",
            "23": "Light snow shower (day)",
            "24": "Light snow",
            "25": "Heavy snow shower (night)",
            "26": "Heavy snow shower (day)",
            "27": "Heavy snow",
            "28": "Thunder shower (night)",
            "29": "Thunder shower (day)",
            "30": "Thunder"
        }
        return sig_code.get(str(self.significantWeatherCode))


class OneHourForecast(Forecast):
    """Forecast that spans 3 hours."""
    screenTemperature: float
    screenDewPointTemperature: float
    precipitationRate: float


class ThreeHourForecast(Forecast):
    """forecast that spans 3 hours."""
    probOfSnow: int
    probOfHeavySnow: int
    probOfRain: int
    probOfHeavyRain: int
    probOfHail: int
    probOfSferics: int



class Dailyforecast(Forecast):
    """forecast that spans 3 hours."""
    time: datetime
    midday10MWindSpeed: float
    midnight10MWindSpeed: int
    midday10MWindDirection: int
    midnight10MWindDirection: int
    midday10MWindGust: float
    midnight10MWindGust: float
    middayVisibility: float
    midnightVisibility: float
    middayRelativeHumidity: float
    midnightRelativeHumidity: float
    middayMslp: int
    midnightMslp: int
    maxUvIndex: int
    daySignificantWeatherCode: float
    nightSignificantWeatherCode: int
    dayMaxScreenTemperature: float
    nightMinScreenTemperature: float
    dayUpperBoundMaxTemp: float
    nightUpperBoundMinTemp: float
    dayLowerBoundMaxTemp: float
    nightLowerBoundMinTemp: float
    dayMaxFeelsLikeTemp: float
    nightMinFeelsLikeTemp: float
    dayUpperBoundMaxFeelsLikeTemp: float
    nightUpperBoundMinFeelsLikeTemp: float
    dayLowerBoundMaxFeelsLikeTemp: float
    nightLowerBoundMinFeelsLikeTemp: float
    dayProbabilityOfPrecipitation: int
    nightProbabilityOfPrecipitation: int
    dayProbabilityOfSnow: int
    nightProbabilityOfSnow: int
    dayProbabilityOfHeavySnow: int
    nightProbabilityOfHeavySnow: int
    dayProbabilityOfRain: int
    nightProbabilityOfRain: int
    dayProbabilityOfHeavyRain: int
    nightProbabilityOfHeavyRain: int
    dayProbabilityOfHail: int
    nightProbabilityOfHail: int
    dayProbabilityOfSferics: int
    nightProbabilityOfSferics: int
