"""Fetch and parse Forecast data."""

import requests
import json
from os import environ
from datetime import datetime, timedelta
from typing import List

from pydantic import BaseModel
from loguru import logger

from weather_app.models import ThreeHourForecast
from weather_app.database import cursor, conn


class Forecasts(BaseModel):
    """Poll and parse forecasts."""

    requestPointDistance: float
    modelRunDate: datetime
    timeSeries: List[ThreeHourForecast]

    def update_database(self):
        model_time = self.modelRunDate.strftime("%Y-%m-%dT%T")
        logger.info(f"Writing {model_time}")

        cursor.execute(
            "INSERT INTO model_run_data(modelRunDate) VALUES (?)",
            (model_time,)
        )
        conn.commit()

        for forecast in self.timeSeries:
            cursor.execute(
                """
                INSERT INTO forecast_data(
                        forecastTime,
                        maxScreenAirTemp,
                        minScreenAirTemp,
                        feelsLikeTemp,
                        significantWeatherCode,
                        probOfPrecipitation,
                        totalPrecipAmount,
                        totalSnowAmount,
                        windSpeed10m,
                        windDirectionFrom10m,
                        windGustSpeed10m,
                        max10mWindGust,
                        visibility,
                        mslp,
                        uvIndex,
                        screenRelativeHumidity,
                        probOfSnow,
                        probOfHeavySnow,
                        probOfRain,
                        probOfHeavyRain,
                        probOfHail,
                        probOfSferics
                    )
                    Values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);
                """,
                (
                    forecast.time,
                    forecast.maxScreenAirTemp,
                    forecast.minScreenAirTemp,
                    forecast.feelsLikeTemp,
                    forecast.significantWeatherCode,
                    forecast.probOfPrecipitation,
                    forecast.totalPrecipAmount,
                    forecast.totalSnowAmount,
                    forecast.windSpeed10m,
                    forecast.windDirectionFrom10m,
                    forecast.windGustSpeed10m,
                    forecast.max10mWindGust,
                    forecast.visibility,
                    forecast.mslp,
                    forecast.uvIndex,
                    forecast.screenRelativeHumidity,
                    forecast.probOfSnow,
                    forecast.probOfHeavySnow,
                    forecast.probOfRain,
                    forecast.probOfHeavyRain,
                    forecast.probOfHail,
                    forecast.probOfSferics
                )
            )
            conn.commit()

    @classmethod
    def poll(cls):
        url = ""
        key = environ.get("DATA_HUB_KEY", "")
        lat = environ.get("WEATHER_LAT", "")
        lon = environ.get("WEATHER_LON", "")
        env = environ.get("WEATHER_APP_ENV", "development")

        if env == "development":
            with open("three-hourly.json", "r") as mock_data:
                data = json.loads(mock_data.read())

        if env == "production":
            url = "https://data.hub.api.metoffice.gov.uk/" \
                  "sitespecific/v0/point/three-hourly" \
                  f"?latitude={lat}&longitude={lon}"
            headers = {"apikey": f"{key}"}
            response = requests.get(url, headers=headers)
            data = response.json()
            if response.status_code > 200:
                logger(f"status_code: {response_code}, url: {url}")
                raise ValueError

        forecast_data = data.get("features", [])[0].get("properties", {})

        return cls(**forecast_data)

    @classmethod
    def validate_cache(cls) -> None:
        cursor.execute("""
            SELECT modelRunDate FROM model_run_data ORDER BY 1 DESC LIMIT 1;
        """)
        result = cursor.fetchone()

        if result is None:
            logger.info("No Model run data in database.")
            cls.poll().update_database()
            return

        last_run = datetime.strptime(result["modelRunDate"],
                                     "%Y-%m-%dT%H:%M:%S")
        since_last_run = datetime.utcnow() - last_run

        if since_last_run > timedelta(hours=3):
            logger.info("Model Run data older than 3 hours. "
                        "Deleting forecast data.")
            cursor.execute("""DELETE FROM forecast_data;""")
            cursor.execute("""DELETE FROM model_run_data""")
            conn.commit()
            cls.poll().update_database()

    @classmethod
    def next_forecast(cls, time: str) -> ThreeHourForecast:
        logger.info(f"Next forecast time: {time}")
        cursor.execute(f"""
            SELECT * FROM forecast_data
            WHERE forecastTime = "{time}"
            limit 1;
        """)
        result = cursor.fetchone()
        if result is None:
            cursor.execute(f"""
                SELECT * FROM forecast_data
                limit 1;
            """)
            result = cursor.fetchone()
            if result is None:
                raise ValueError
        return ThreeHourForecast(**result)

