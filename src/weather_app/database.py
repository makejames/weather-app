"""Create and handle database connections."""

import sqlite3

def dict_factory(cursor, row) -> dict:
    fields = [column[0] for column in cursor.description]
    return {key: value for key, value in zip(fields, row)}

conn = sqlite3.connect(":memory:", check_same_thread=False)
conn.row_factory = dict_factory
cursor = conn.cursor()
cursor.execute("""
    CREATE TABLE IF NOT EXISTS forecast_data (
        forecastTime            DATETIME PRIMARY KEY,
        maxScreenAirTemp        FLOAT,
        minScreenAirTemp        FLOAT,
        feelsLikeTemp           FLOAT,
        significantWeatherCode  INTEGER,
        probOfPrecipitation     FLOAT,
        probOfSnow              INTEGER,
        probOfHeavySnow         INTEGER,
        probOfRain              INTEGER,
        probOfHeavyRain         INTEGER,
        probOfHail              INTEGER,
        probOfSferics           INTEGER,
        totalPrecipAmount       FLOAT,
        totalSnowAmount         FLOAT,
        windSpeed10m            FLOAT,
        windDirectionFrom10m    INTEGER,
        windGustSpeed10m        FLOAT,
        max10mWindGust          FLOAT,
        visibility              INTEGER,
        mslp                    INTEGER,
        uvIndex                 INTEGER,
        screenRelativeHumidity  FLOAT
    );
""")
conn.commit()

cursor.execute("""
    CREATE TABLE IF NOT EXISTS model_run_data (
        modelRunDate TEXT PRIMARY KEY
    );
""")
conn.commit()
