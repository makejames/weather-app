"""Declare the application routes."""

from datetime import datetime, timedelta

from fastapi import FastAPI, Request
from fastapi.staticfiles import StaticFiles
from fastapi.responses import HTMLResponse, FileResponse
from fastapi.templating import Jinja2Templates

from asgi_correlation_id import CorrelationIdMiddleware

from loguru import logger

from weather_app.database import conn, cursor

from weather_app.forecasts import Forecasts

import logging
from typing import Any

import structlog
from asgi_correlation_id import correlation_id


def add_correlation(
    logger: logging.Logger, method_name: str, event_dict: dict[str, Any]
) -> dict[str, Any]:
    """Add request id to log message."""
    if request_id := correlation_id.get():
        event_dict["request_id"] = request_id
    return event_dict


structlog.configure(
    processors=[
        add_correlation,
        structlog.stdlib.filter_by_level,
        structlog.stdlib.add_logger_name,
        structlog.stdlib.add_log_level,
        structlog.processors.TimeStamper(fmt="%Y-%m-%d %H:%M.%S"),
        structlog.processors.StackInfoRenderer(),
        structlog.processors.format_exc_info,
        structlog.processors.JSONRenderer(),
    ],
    wrapper_class=structlog.stdlib.BoundLogger,
    logger_factory=structlog.stdlib.LoggerFactory(),
    cache_logger_on_first_use=True,
)

logger = structlog.get_logger()

app = FastAPI()
app.add_middleware(CorrelationIdMiddleware)
templates = Jinja2Templates(directory="./src/weather_app/templates")

app.mount("/static", StaticFiles(directory="./src/weather_app/static"),
          name="static files")

@app.get("/", response_class=HTMLResponse)
async def index_html(request: Request):
    return templates.TemplateResponse(request=request, name="home.html",
                                      context={})

@app.get("/about", response_class=HTMLResponse)
async def index_html(request: Request):
    return templates.TemplateResponse(request=request, name="about.html",
                                      context={})

@app.get("/forecast" , response_class=HTMLResponse)
async def forecast(request: Request):
    Forecasts.validate_cache()
    now = datetime.utcnow()
    nearest_3_hrs = (now.hour // 3) * 3
    nearly_now = now.replace(hour=nearest_3_hrs)
    time = nearly_now.strftime("%Y-%m-%d %H:00:00+00:00")
    logger.info(f"Forecast requested: {time}")
    forecast = Forecasts.next_forecast(time)
    return templates.TemplateResponse(
        request=request,
        name="forecast.html",
        context={"forecast": forecast},
    )

@app.get("/forecast/{time}" , response_class=HTMLResponse)
async def forecast_time(request: Request, time: str):
    Forecasts.validate_cache()
    if time is None:
        now = datetime.utcnow()
        nearest_3_hrs = (now.hour // 3) * 3
        nearly_now = now.replace(hour=nearest_3_hrs)
        time = nearly_now.strftime("%Y-%m-%d %H:00:00+00:00")
    time = time.replace("%20", " ")
    time = time.replace("%2B", "+")
    time = time.replace("%3A", ":")
    logger.info(f"Forecast requested: {time}")
    forecast = Forecasts.next_forecast(time)
    return templates.TemplateResponse(
        request=request,
        name="forecast.html",
        context={"forecast": forecast},
    )

@app.get("/summary" , response_class=HTMLResponse)
async def summary(request: Request):
    Forecasts.validate_cache()
    now = datetime.utcnow()
    nearest_3_hrs = (now.hour // 3) * 3
    nearly_now = now.replace(hour=nearest_3_hrs)
    time = nearly_now.strftime("%Y-%m-%d %H:00:00+00:00")
    logger.info(f"Forecast requested: {time}")
    forecast = Forecasts.next_forecast(time)
    forecast_list = [forecast, ]

    x = 0

    while x < 3:
        x += 1
        next_time_step = forecast_list[-1].next_time_step(3)
        logger.info(f"Forecast requested: {next_time_step}")
        forecast_list.append(Forecasts.next_forecast(next_time_step))

    date = [
        {
            "time": forecast_list[0].time,
            "length": 4
        },
    ]
    date_panel_length = 4
    if forecast_list[0].time.hour > 12:
        date.append(
            {
                "time": forecast_list[3].time,
                "length": int((forecast_list[0].time.hour - 12) / 3)
            }
        )
        date[0]["length"] = 4 - int((forecast_list[0].time.hour - 12) / 3)

    return templates.TemplateResponse(
        request=request,
        name="summary.html",
        context={"forecast_list": forecast_list,
                 "date": date},
    )

@app.get("/summary/{time}" , response_class=HTMLResponse)
async def summary_timestep(request: Request, time):
    Forecasts.validate_cache()
    time = time.replace("%20", " ")
    time = time.replace("%2B", "+")
    time = time.replace("%3A", ":")
    logger.info(f"Forecast requested: {time}")
    forecast = Forecasts.next_forecast(time)
    forecast_list = [forecast, ]

    x = 0

    while x < 3:
        x += 1
        next_time_step = forecast_list[-1].next_time_step(3)
        logger.info(f"Forecast requested: {next_time_step}")
        forecast_list.append(Forecasts.next_forecast(next_time_step))

    date = [
        {
            "time": forecast_list[0].time,
            "length": 4
        },
    ]
    date_panel_length = 4
    if forecast_list[0].time.hour > 12:
        date.append(
            {
                "time": forecast_list[3].time,
                "length": int((forecast_list[0].time.hour - 12) / 3)
            }
        )
        date[0]["length"] = 4 - int((forecast_list[0].time.hour - 12) / 3)

    return templates.TemplateResponse(
        request=request,
        name="summary.html",
        context={"forecast_list": forecast_list,
                 "date": date}
    )
