#!/bin/bash

rm -rf /opt/weather-app
mkdir /opt/weather-app

# Copy application files
cp -r weather_app start.sh requirements.txt README.md /opt/weather-app/

# install python dependencies
python3.11 -m pip install -r /opt/weather-app/requirements.txt

# Copy nginx files
cp weather-app /etc/nginx/sites-available
ln /etc/nginx/sites-available/weather-app /etc/nginx/sites-enabled/weather-app

# Copy service files
cp weather-app.service /etc/systemd/system
systemctl daemon-reload
