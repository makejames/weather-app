ARG PYTHON_BASE=3.11-slim

# Build step
FROM python:$PYTHON_BASE AS builder

RUN pip install -U pdm
ENV PDM_CHECK_UPDATE=false

COPY pyproject.toml pdm.lock README.md /weather-app/
COPY src/ /weather-app/src

WORKDIR /weather-app

RUN pdm install --check --prod --no-editable

# run stage
FROM python:$PYTHON_BASE

COPY --from=builder /weather-app/.venv/ /weather-app/.venv

ENV PATH="/weather-app/.venv/bin:$PATH"

COPY src /weather-app/src

CMD ["gunicorn", "-w", "4", "-k", "uvicorn.workers.UvicornWorker", "-b", "0.0.0.0:8000", "weather_app.app:app"]
