#!/bin/bash

export PATH=$PATH:/opt/weather-app/.venv/bin
echo "starting production server"
source /opt/weather-app/.env
gunicorn -w 4 -k uvicorn.workers.UvicornWorker -b $WEATHER_APP_HOST:$WEATHER_APP_PORT weather_app.app:app
