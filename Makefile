SHELL=/bin/bash

dev:
	poetry env use python3.11
	poetry install

serve: dev
	poetry run uvicorn weather_app.app:app --host 127.0.0.1 --port 3000

requirements:
	poetry export --without-hashes --format=requirements.txt > requirements.txt
